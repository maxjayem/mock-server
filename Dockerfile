# Use the base image
FROM muonsoft/openapi-mock

# Copy the OpenAPI specification file into the container
COPY ./libs/api/src/openapi-spec.yaml /etc/openapi/openapi-spec.yaml

# Set environment variables
ENV OPENAPI_MOCK_SPECIFICATION_URL /etc/openapi/openapi-spec.yaml
ENV OPENAPI_MOCK_USE_EXAMPLES if_present
ENV OPENAPI_MOCK_CORS_ENABLED true

# Expose port
EXPOSE 10000
